# Prueba Iot Enerbit

En esta prueba se debe realizar un proyecto que implemente modbus TCP y conexion a IoThub haciendo uso del SDK de esp_azure.
Dentro de la configuración modbus se deben leer las siguientes variables:

Example parameters definition:
--------------------------------------------------------------------------------------------------
| Slave Address       | Characteristic ID    | Characteristic name  | Description                 |
|---------------------|----------------------|----------------------|-----------------------------|
| MB_DEVICE_ADDR1     | CID_HOLD_DATA_0,     | Voltage L1-N         | Voltaje de linea1-neutro    |
| MB_DEVICE_ADDR1     | CID_HOLD_DATA_1,     | Voltage L2-N         | Voltaje de linea2-neutro    |
| MB_DEVICE_ADDR1     | CID_HOLD_DATA_2,     | Voltage L3 -N        | Voltaje de linea3-neutro    |
| MB_DEVICE_ADDR1     | CID_HOLD_DATA_3,     | Current  L1          | Corriente de linea1         |
| MB_DEVICE_ADDR1     | CID_HOLD_DATA_4      | Current  L2          | Corriente de linea2         |
| MB_DEVICE_ADDR1     | CID_HOLD_DATA_5      | Current  L3          | Corriente de linea2         |
| MB_DEVICE_ADDR1     | CID_HOLD_DATA_6,     | FP  L1               | Factor de potencia linea1   |
| MB_DEVICE_ADDR1     | CID_HOLD_DATA_7      | FP  L2               | Factor de potencia linea2   |
| MB_DEVICE_ADDR1     | CID_HOLD_DATA_8      | FP  L3               | Factor de potencia linea2   |
| MB_DEVICE_ADDR1     | CID_HOLD_DATA_9      | Energy               | Energia acomuluda importada |
--------------------------------------------------------------------------------------------------
Nota: La dirección del esclavo es la misma para todos los parámetros, por ejemplo, prueba, pero se puede cambiar en la tabla "Diccionario de datos (objetos) de ejemplo" del ejemplo maestro para abordar los parámetros de otros esclavos.
El parámetro Kconfig ```Dirección de esclavo Modbus``` - CONFIG_MB_SLAVE_ADDR en el ejemplo de esclavo se puede configurar para crear un segmento de esclavo múltiple Modbus.

Modbus multi slave segment connection schematic:
```
    MB_DEVICE_ADDR1
    -------------
    |           |   
    |  Slave 1  |---<>--+
    |           |       |
    -------------       |
    MB_DEVICE_ADDR2     |
    -------------       |        -------------
    |           |       |        |           |
    |  Slave  2 |---<>--+---<>---|  Master   |
    |           |       |        |           |
    -------------       |        -------------
    MB_DEVICE_ADDR3     |
    -------------     Network (Ethernet or WiFi connection)
    |           |       |
    |  Slave 3  |---<>--+
    |           |
    -------------
```

## Hardware requerido :
Opcion 1:
PC (aplicación Modbus TCP Slave) + placa de desarrollo ESP32(-S2) con ejemplo de modbus_tcp_slave.

Opcion 2:
Varias placas ESP32(-S2) mostraron el software de ejemplo modbus_tcp_slave para representar dispositivos esclavos. Las direcciones IP esclavas para cada placa deben configurarse en el menú `Configuración de ejemplo de Modbus` de acuerdo con la tabla de comunicación del ejemplo.
Una placa de desarrollo ESP32(-S2) debe actualizarse con el ejemplo modbus_master y conectarse a la misma red. Todas las placas requieren la configuración de los ajustes de red como se describe en `examples/common_components/protocol_examples_common`.

## Como configurar y usar el proyecto:

### Configure the application
Inicie el siguiente comando para configurar la configuración:
```
idf.py menuconfig
```

Los parámetros de comunicación de la pila Modbus permiten configurarla adecuadamente, pero normalmente basta con utilizar la configuración predeterminada.
Consulte la cadena de ayuda de parámetros para obtener más información.
Hay tres formas de configurar cómo el ejemplo maestro obtendrá las direcciones IP esclavas en la red:
* Habilitar la opción CONFIG_MB_MDNS_IP_RESOLVER permite consultar los servicios Modbus proporcionados por los esclavos Modbus en la red y configurar automáticamente la tabla de IP. Esto requiere activar la misma opción para cada esclavo con una dirección de esclavo modbus única configurada en el menú "Configuración de ejemplo de Modbus".
* Habilite la opción CONFIG_MB_SLAVE_IP_FROM_STDIN para definir las direcciones IP de los esclavos manualmente. Para ingresar las direcciones IP, espere el mensaje y escriba la cadena con la dirección IP en el siguiente formato. Mensaje: "Esperando IPN desde stdin:", luego ingrese la dirección IP del esclavo para conectarse: "IPN=192.168.1.21", donde N = (dirección del esclavo configurado - 1).
* Configure las direcciones esclavas manualmente como se muestra a continuación:
```
char* slave_ip_address_table[MB_DEVICE_COUNT] = {
    "192.168.1.21",     // Address corresponds to MB_DEVICE_ADDR1 and set to predefined value by user
    "192.168.1.22",     // Address corresponds to MB_DEVICE_ADDR2 of slave device in the Modbus data dictionary
    NULL                // Marker of end of list
    };
```

en el menú de configuración se debe configurar el SSID y PASSWORD de la red a donde se van a conectar los dispositivos esclavos.
* Example Connection Configuration  ---> 
 * * SSID -> nombre de la red wifi.
 * * PASSWORD -> contraseña.

Dentro del archivo azure.c se debe pegar la url de la cadena de conexion del dispositivo IoT, definido con la siguiente Macro ubicada en la linea #24 de este archivo.:
```
 #define USER_CONNECTION_STRING2 "HostName=i="  
```

### Build and flash software of master device
Build the project and flash it to the board, then run monitor tool to view serial output:
```
idf.py -p PORT flash monitor
```

(To exit the serial monitor, type ``Ctrl-]``.)

See the Getting Started Guide for full steps to configure and use ESP-IDF to build projects.

## Example Output
Example output of the application:
```
I (1860) wifi:AP's beacon interval = 102400 us, DTIM period = 1
I (3310) esp_netif_handlers: example_connect: sta ip: 192.168.1.4, mask: 255.255.255.0, gw: 192.168.1.254
I (3310) example_connect: Got IPv4 event: Interface "example_connect: sta" address: 192.168.1.4
I (3320) example_connect: Connected to example_connect: sta
I (3320) example_connect: - IPv4 address: 192.168.1.4
I (3330) wifi:Set ps type: 0

E (3330) MASTER_TEST: No hay ping..
I (4570) check_connection: 64 bytes from 13.35.116.35 icmp_seq=1 ttl=246 time=182 ms
I (5450) check_connection: 64 bytes from 13.35.116.35 icmp_seq=2 ttl=246 time=65 ms
I (6440) check_connection: 64 bytes from 13.35.116.35 icmp_seq=3 ttl=246 time=53 ms
I (7440) check_connection: 64 bytes from 13.35.116.35 icmp_seq=4 ttl=246 time=52 ms
I (8440) check_connection: 64 bytes from 13.35.116.35 icmp_seq=5 ttl=246 time=48 ms
I (9390) check_connection: 
--- 13.35.116.35 ping statistics ---
I (9390) check_connection: 5 packets transmitted, 5 received, 0% packet loss, time 400ms

I (9390) MASTER_TEST: Si hay ping..
I (9390) uart: ESP_INTR_FLAG_IRAM flag not set while CONFIG_UART_ISR_IN_IRAM is enabled, flag updated
I (9400) MASTER_TEST: Leave IP(0) = [192.168.1.3] set manually.
I (9420) MASTER_TEST: Leave IP(1) = [192.168.1.3] set manually.
I (9420) MASTER_TEST: Leave IP(2) = [192.168.1.10] set manually.
I (9430) MASTER_TEST: Leave IP(3) = [192.168.1.5] set manually.
I (9430) MASTER_TEST: IP(4) is not set in the table.
I (9440) MASTER_TEST: Configured 4 IP addresse(s).
I (9440) MASTER_TEST: Modbus master stack initialized...
I (9460) MB_TCP_MASTER_PORT: TCP master stack initialized.
I (9460) MB_TCP_MASTER_PORT: Host[IP]: "192.168.1.3"[192.168.1.3]
I (9470) MB_TCP_MASTER_PORT: Add slave IP: 192.168.1.3
I (9470) MB_TCP_MASTER_PORT: Connecting to slaves...
-.-.-.-I (9610) MB_TCP_MASTER_PORT: Connected 1 slaves, start polling...
I (10160) MASTER_TEST: Start modbus test...
I (10160) Cloud: Connection String: HostName=iot-hub-test-mp.azure-devices.net;DeviceId=iot-mp-device;SharedAccessKey=6mJUCwebROuNWCwYan5+xGIgoCRSd/vfDAIoTODBr4k=
Initializing SNTP
ESP platform sntp inited!
Time is not set yet. Connecting to WiFi and getting time over NTP. timeinfo.tm_year:70
Waiting for system time to be set... tm_year:0[times:1]
I (10230) MASTER_TEST: Characteristic #0 Voltage L1-N (Volts) value = 130.009888 (0x43020288) read successful.
I (10310) MASTER_TEST: Characteristic #1 Voltage L2-N (Volts) value = 80.120003 (0x42a03d71) read successful.
I (10370) MASTER_TEST: Characteristic #2 Voltage L3-N (Volts) value = 75.129997 (0x4296428f) read successful.
I (10440) MASTER_TEST: Characteristic #3 Current L1 (Amps) value = 46.669998 (0x423aae14) read successful.
I (10500) MASTER_TEST: Characteristic #4 Current L2 (Amps) value = 95.779999 (0x42bf8f5c) read successful.
I (10580) MASTER_TEST: Characteristic #5 Current L3 (Amps) value = 23.100000 (0x41b8cccd) read successful.
I (10650) MASTER_TEST: Characteristic #6 Power Factor L1 (-) value = 1.000000 (0x3f800000) read successful.
I (10720) MASTER_TEST: Characteristic #7 Power Factor L2 (-) value = 0.000000 (0x0) read successful.
I (10790) MASTER_TEST: Characteristic #8 Power Factor L3 (-) value = 1.971573 (0x3ffc5c84) read successful.
I (10790) MASTER_TEST: envio de la cola con la telemetria
E (10860) MB_CONTROLLER_MASTER: mbc_master_get_parameter(85): Master get parameter failure, error=(0x108) (ESP_ERR_INVALID_RESPONSE).
E (10860) MASTER_TEST: Characteristic #9 (Cumulative Energy imported) read fail, err = 264 (ESP_ERR_INVALID_RESPONSE).
I (11430) MASTER_TEST: Characteristic #0 Voltage L1-N (Volts) value = 131.109894 (0x43031c22) read successful.
I (11500) MASTER_TEST: Characteristic #1 Voltage L2-N (Volts) value = 80.120003 (0x42a03d71) read successful.
I (11570) MASTER_TEST: Characteristic #2 Voltage L3-N (Volts) value = 75.129997 (0x4296428f) read successful.
I (11640) MASTER_TEST: Characteristic #3 Current L1 (Amps) value = 46.669998 (0x423aae14) read successful.
I (11710) MASTER_TEST: Characteristic #4 Current L2 (Amps) value = 95.779999 (0x42bf8f5c) read successful.
I (11780) MASTER_TEST: Characteristic #5 Current L3 (Amps) value = 23.100000 (0x41b8cccd) read successful.
I (11850) MASTER_TEST: Characteristic #6 Power Factor L1 (-) value = 1.000000 (0x3f800000) read successful.
I (11920) MASTER_TEST: Characteristic #7 Power Factor L2 (-) value = 0.000000 (0x0) read successful.
I (11990) MASTER_TEST: Characteristic #8 Power Factor L3 (-) value = 1.971573 (0x3ffc5c84) read successful.
I (11990) MASTER_TEST: envio de la cola con la telemetria
E (12060) MB_CONTROLLER_MASTER: mbc_master_get_parameter(85): Master get parameter failure, error=(0x108) (ESP_ERR_INVALID_RESPONSE).
E (12060) MASTER_TEST: Characteristic #9 (Cumulative Energy imported) read fail, err = 264 (ESP_ERR_INVALID_RESPONSE).
Waiting for system time to be set... tm_year:70[times:2]
I (12640) MASTER_TEST: Characteristic #0 Voltage L1-N (Volts) value = 132.209900 (0x430435bc) read successful.
I (12710) MASTER_TEST: Characteristic #1 Voltage L2-N (Volts) value = 80.120003 (0x42a03d71) read successful.
I (12790) MASTER_TEST: Characteristic #2 Voltage L3-N (Volts) value = 75.129997 (0x4296428f) read successful.
I (12860) MASTER_TEST: Characteristic #3 Current L1 (Amps) value = 46.669998 (0x423aae14) read successful.
I (12950) MASTER_TEST: Characteristic #4 Current L2 (Amps) value = 95.779999 (0x42bf8f5c) read successful.
I (13040) MASTER_TEST: Characteristic #5 Current L3 (Amps) value = 23.100000 (0x41b8cccd) read successful.
I (13140) MASTER_TEST: Characteristic #6 Power Factor L1 (-) value = 1.000000 (0x3f800000) read successful.
I (13220) MASTER_TEST: Characteristic #7 Power Factor L2 (-) value = 0.000000 (0x0) read successful.
I (13310) MASTER_TEST: Characteristic #8 Power Factor L3 (-) value = 1.971573 (0x3ffc5c84) read successful.
I (13310) MASTER_TEST: envio de la cola con la telemetria
E (13380) MB_CONTROLLER_MASTER: mbc_master_get_parameter(85): Master get parameter failure, error=(0x108) (ESP_ERR_INVALID_RESPONSE).
E (13380) MASTER_TEST: Characteristic #9 (Cumulative Energy imported) read fail, err = 264 (ESP_ERR_INVALID_RESPONSE).
I (14030) MASTER_TEST: Characteristic #0 Voltage L1-N (Volts) value = 133.309906 (0x43054f56) read successful.
I (14100) MASTER_TEST: Characteristic #1 Voltage L2-N (Volts) value = 80.120003 (0x42a03d71) read successful.
I (14180) MASTER_TEST: Characteristic #2 Voltage L3-N (Volts) value = 75.129997 (0x4296428f) read successful.
Waiting for system time to be set... tm_year:70[times:3]
I (14260) MASTER_TEST: Characteristic #3 Current L1 (Amps) value = 46.669998 (0x423aae14) read successful.
I (14350) MASTER_TEST: Characteristic #4 Current L2 (Amps) value = 95.779999 (0x42bf8f5c) read successful.
I (14430) MASTER_TEST: Characteristic #5 Current L3 (Amps) value = 23.100000 (0x41b8cccd) read successful.
I (14500) MASTER_TEST: Characteristic #6 Power Factor L1 (-) value = 1.000000 (0x3f800000) read successful.
I (14590) MASTER_TEST: Characteristic #7 Power Factor L2 (-) value = 0.000000 (0x0) read successful.
I (14690) MASTER_TEST: Characteristic #8 Power Factor L3 (-) value = 1.971573 (0x3ffc5c84) read successful.
I (14690) MASTER_TEST: envio de la cola con la telemetria
E (14790) MB_CONTROLLER_MASTER: mbc_master_get_parameter(85): Master get parameter failure, error=(0x108) (ESP_ERR_INVALID_RESPONSE).
E (14790) MASTER_TEST: Characteristic #9 (Cumulative Energy imported) read fail, err = 264 (ESP_ERR_INVALID_RESPONSE).
I (15360) MASTER_TEST: Characteristic #0 Voltage L1-N (Volts) value = 134.409912 (0x430668f0) read successful.
I (15460) MASTER_TEST: Characteristic #1 Voltage L2-N (Volts) value = 80.120003 (0x42a03d71) read successful.
I (15560) MASTER_TEST: Characteristic #2 Voltage L3-N (Volts) value = 75.129997 (0x4296428f) read successful.
I (15640) MASTER_TEST: Characteristic #3 Current L1 (Amps) value = 46.669998 (0x423aae14) read successful.
I (15730) MASTER_TEST: Characteristic #4 Current L2 (Amps) value = 95.779999 (0x42bf8f5c) read successful.
I (15800) MASTER_TEST: Characteristic #5 Current L3 (Amps) value = 23.100000 (0x41b8cccd) read successful.
I (15870) MASTER_TEST: Characteristic #6 Power Factor L1 (-) value = 1.000000 (0x3f800000) read successful.
I (15940) MASTER_TEST: Characteristic #7 Power Factor L2 (-) value = 0.000000 (0x0) read successful.
I (16050) MASTER_TEST: Characteristic #8 Power Factor L3 (-) value = 1.971573 (0x3ffc5c84) read successful.
E (16110) MB_CONTROLLER_MASTER: mbc_master_get_parameter(85): Master get parameter failure, error=(0x108) (ESP_ERR_INVALID_RESPONSE).
E (16110) MASTER_TEST: Characteristic #9 (Cumulative Energy imported) read fail, err = 264 (ESP_ERR_INVALID_RESPONSE).
I (16190) platform: The current date/time is: Mon Jan 15 19:36:31 2024
I (16190) Cloud: INFO: there is a msg into queue telemetry

E (16190) Cloud: Fecha NOw: 2024-1-15 19:36:31

W (16210) Cloud: JSON: {
        "Msg":  "Telemetria",
        "Time": "2024-1-15 19:36:31",
        "mediciones":   [{
                        "area": "Foundry",
                        "VL1N": 130.0098876953125,
                        "VL2N": 80.120002746582031,
                        "VL3N": 75.129997253417969,
                        "CL1":  46.669998168945312,
                        "CL2":  95.779998779296875,
                        "CL3":  23.100000381469727,
                        "FP1":  1,
                        "FP2":  0,
                        "FP3":  1.9715733528137207,
                        "EAC":  0
                }, {
                        "area": "Molding",
                        "VL1N": 130.0098876953125,
                        "VL2N": 80.120002746582031,
                        "VL3N": 75.129997253417969,
                        "CL1":  46.669998168945312,
                        "CL2":  95.779998779296875,
                        "CL3":  23.100000381469727,
                        "FP1":  1,
                        "FP2":  0,
                        "FP3":  1.9715733528137207,
                        "EAC":  0
                }, {
                        "area": "Pneumatic press",
                        "VL1N": 130.0098876953125,
                        "VL2N": 80.120002746582031,
                        "VL3N": 75.129997253417969,
                        "CL1":  46.669998168945312,
                        "CL2":  95.779998779296875,
                        "CL3":  23.100000381469727,
                        "FP1":  1,
                        "FP2":  0,
                        "FP3":  1.9715733528137207,
                        "EAC":  0
                }, {
                        "area": "Warehouse",
                        "VL1N": 130.0098876953125,
                        "VL2N": 80.120002746582031,
                        "VL3N": 75.129997253417969,
                        "CL1":  46.669998168945312,
                        "CL2":  95.779998779296875,
                        "CL3":  23.100000381469727,
                        "FP1":  1,
                        "FP2":  0,
                        "FP3":  1.9715733528137207,
                        "EAC":  0
                }]
}

I (16320) Cloud: IoTHubClient_LL_SendEventAsync accepted message[0] for transmission to IoT Hub.

-> 19:36:33 CONNECT | VER: 4 | KEEPALIVE: 240 | FLAGS: 192 | USERNAME: iot-hub-test-mp.azure-devices.net/iot-mp-device/?api-version=2020-09-30&DeviceClientType=iothubclient%2f1.8.0%20(native%3b%20freertos%3b%20esp%20platform) | PWD: XXXX | CLEAN: 0
<- 19:36:33 CONNACK | SESSION_PRESENT: true | RETURN_CODE: 0x0
I (18290) Cloud:

Connection Status result:IOTHUB_CLIENT_CONNECTION_AUTHENTICATED, Connection Status reason: IOTHUB_CLIENT_CONNECTION_OK

