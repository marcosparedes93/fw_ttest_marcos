#include "user_common.h"
#include <stdio.h>
#include <stdlib.h>
#include "esp_log.h"
#include "iothub_client.h"
#include "iothub_device_client_ll.h"
#include "iothub_client_options.h"
#include "iothub_message.h"
#include "azure_c_shared_utility/threadapi.h"
#include "azure_c_shared_utility/crt_abstractions.h"
#include "azure_c_shared_utility/platform.h"
#include "azure_c_shared_utility/shared_util_options.h"
#include "iothubtransportmqtt.h"
#include "iothub_client_options.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "cJSON/cJSON.h"
#include "freertos/queue.h"


//#define USER_CONNECTION_STRING  "HostName=iot-hub-test-mp.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=SOh4s2SAiDlocr4F1aNsUG9ReuBcZ4zqvAIoTKgrnl8="  //conection para el app de mensajes

#define USER_CONNECTION_STRING2 "HostName=iot-hub-test-mp.azure-devices.net;DeviceId=iot-mp-device;SharedAccessKey=6mJUCwebROuNWCwYan5+xGIgoCRSd/vfDAIoTODBr4k="


static char *TAG = "azure";



char AZURE_IOT_CONNECTION_STRING[200];

#ifdef MBED_BUILD_TIMESTAMP
#define SET_TRUSTED_CERT_IN_SAMPLES
#endif // MBED_BUILD_TIMESTAMP

#ifdef SET_TRUSTED_CERT_IN_SAMPLES
#include "certs.h"
#endif // SET_TRUSTED_CERT_IN_SAMPLES

static int callbackCounter;
static char msgText[1024];

extern QueueHandle_t Q_Measurement;
extern QueueHandle_t Q_Event;

typedef struct EVENT_INSTANCE_TAG
{
    IOTHUB_MESSAGE_HANDLE messageHandle;
    size_t messageTrackingId; // For tracking the messages within the user callback.
} EVENT_INSTANCE;

static IOTHUBMESSAGE_DISPOSITION_RESULT ReceiveMessageCallback(IOTHUB_MESSAGE_HANDLE message, void *userContextCallback)
{
    int *counter = (int *)userContextCallback;
    const char *buffer;
    size_t size;

    if (IoTHubMessage_GetByteArray(message, (const unsigned char **)&buffer, &size) != IOTHUB_MESSAGE_OK)
    {
        printf("Unable to retrieve the message data\r\n");
    }
    else
    {
        ESP_LOGI("Cloud", "Received message: Data: %.*s & Size=%d\r\n", (int)size, buffer, (int)size);
    }
    return IOTHUBMESSAGE_ACCEPTED;
}

void connection_status_callback(IOTHUB_CLIENT_CONNECTION_STATUS result, IOTHUB_CLIENT_CONNECTION_STATUS_REASON reason, void *userContextCallback)
{
    ESP_LOGI("Cloud", "\n\nConnection Status result:%s, Connection Status reason: %s\n\n", MU_ENUM_TO_STRING(IOTHUB_CLIENT_CONNECTION_STATUS, result),
             MU_ENUM_TO_STRING(IOTHUB_CLIENT_CONNECTION_STATUS_REASON, reason));
}

static void SendConfirmationCallback(IOTHUB_CLIENT_CONFIRMATION_RESULT result, void *userContextCallback)
{
    EVENT_INSTANCE *eventInstance = (EVENT_INSTANCE *)userContextCallback;
    size_t id = eventInstance->messageTrackingId;

    if (result == IOTHUB_CLIENT_CONFIRMATION_OK)
    {
        ESP_LOGI("Cloud", "Confirmation[%d] received for message tracking id = %d with result = %s\r\n", callbackCounter, (int)id, MU_ENUM_TO_STRING(IOTHUB_CLIENT_CONFIRMATION_RESULT, result));
        /* Some device specific action code goes here... */
        // ESP_LOGI("cloud","Server response");
        callbackCounter++;
    }
    IoTHubMessage_Destroy(eventInstance->messageHandle);
}

char *create_telemetry(telemetry_t* data){
    //creamos Json de telemetria  
                    char *stringJS = NULL;                  
                    cJSON *root = NULL;
                    cJSON *Msg = NULL;
                    cJSON *Time = NULL;
                    cJSON *mediciones = NULL; 
                    cJSON *medicion = NULL;                      
                    cJSON *area = NULL;
                    cJSON *VL1N = NULL;
                    cJSON *VL2N = NULL;
                    cJSON *VL3N = NULL;
                    cJSON *CL1 = NULL;
                    cJSON *CL2 = NULL;
                    cJSON *CL3 = NULL;
                    cJSON *FP1 = NULL;                    
                    cJSON *FP2 = NULL;
                    cJSON *FP3 = NULL;
                    cJSON *EAC = NULL;
                    size_t index = 0;
                    cJSON *Telemetry = cJSON_CreateObject();
                    Msg = cJSON_CreateString("Telemetria");
                    cJSON_AddItemToObject(Telemetry, "Msg", Msg);

                    time_t now;
                    struct tm timeinfo;
                    time(&now);
                    localtime_r(&now, &timeinfo);
                    char strTime[35];
                    sprintf(strTime, "%d-%d-%d %d:%d:%d", timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
                    ESP_LOGE("Cloud", "Fecha NOw: %s\n", strTime); 
                    Time = cJSON_CreateString(strTime);
                    cJSON_AddItemToObject(Telemetry, "Time", Time);  

                    mediciones = cJSON_CreateArray();
                    cJSON_AddItemToObject(Telemetry, "mediciones", mediciones);                    
                    for (index = 0; index < 4; index++)
                    {
                        medicion = cJSON_CreateObject();
                        cJSON_AddItemToArray(mediciones, medicion);
                        switch (index)
                        {
                        case 0:
                            area = cJSON_CreateString("Foundry");
                            break;
                        case 1:
                            area = cJSON_CreateString("Molding");
                            break;
                        case 2:
                            area = cJSON_CreateString("Pneumatic press");
                            break;
                        case 3:
                            area = cJSON_CreateString("Warehouse");
                            break;                        
                        }
                        
                        cJSON_AddItemToObject(medicion, "area", area);
                        VL1N = cJSON_CreateNumber(data->VL1N);
                        cJSON_AddItemToObject(medicion, "VL1N", VL1N);
                        VL2N = cJSON_CreateNumber(data->VL2N);
                        cJSON_AddItemToObject(medicion, "VL2N", VL2N);
                        VL3N = cJSON_CreateNumber(data->VL3N);
                        cJSON_AddItemToObject(medicion, "VL3N", VL3N);
                        CL1 = cJSON_CreateNumber(data->CL1);
                        cJSON_AddItemToObject(medicion, "CL1", CL1);
                        CL2 = cJSON_CreateNumber(data->CL2);
                        cJSON_AddItemToObject(medicion, "CL2", CL2);
                        CL3 = cJSON_CreateNumber(data->CL3);
                        cJSON_AddItemToObject(medicion, "CL3", CL3);
                        FP1 = cJSON_CreateNumber(data->FP1);
                        cJSON_AddItemToObject(medicion, "FP1", FP1);
                        FP2 = cJSON_CreateNumber(data->FP2);
                        cJSON_AddItemToObject(medicion, "FP2", FP2);
                        FP3 = cJSON_CreateNumber(data->FP3);
                        cJSON_AddItemToObject(medicion, "FP3", FP3);
                        EAC = cJSON_CreateNumber(data->EAC);
                        cJSON_AddItemToObject(medicion, "EAC", EAC);                       
                        
                    }

                    stringJS = cJSON_Print(Telemetry);
                    if (stringJS == NULL)
                    {                        
                        ESP_LOGE("Cloud", "Failed to create json telemetry.\n");
                    }
                    cJSON_Delete(Telemetry);                    
                    return stringJS;
}

char *create_event_azure(eventMsg_t* msg){
    //creamos Json de eventos  
                    char *stringJS = NULL; 
                    cJSON *Msg = NULL;
                    cJSON *Time = NULL;
                    cJSON *Descript = NULL; 
                    cJSON *value = NULL;                      
                    cJSON *parameter = NULL;                   
                    size_t index = 0;
                    cJSON *Event = cJSON_CreateObject();
                    Msg = cJSON_CreateString("Evento");
                    cJSON_AddItemToObject(Event, "Msg", Msg);

                    time_t now;
                    struct tm timeinfo;
                    time(&now);
                    localtime_r(&now, &timeinfo);
                    char strTime[35];
                    sprintf(strTime, "%d-%d-%d %d:%d:%d", timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
                    ESP_LOGE("Cloud", "Fecha NOw: %s\n", strTime); 
                    Time = cJSON_CreateString(strTime);
                    cJSON_AddItemToObject(Event, "Time", Time);  

                    switch (msg->cid)
                    {
                        case 0:
                            Descript = cJSON_CreateString("Voltaje L1N supera el umbral");
                            break;
                        case 1:
                            Descript = cJSON_CreateString("Voltaje L2N supera el umbral");
                            break;
                        case 2:
                            Descript = cJSON_CreateString("Voltaje L3N supera el umbral");
                            break;
                        case 3:
                            Descript = cJSON_CreateString("Corriente L1 supera el umbral");
                            break;                        
                    }
                    cJSON_AddItemToObject(Event, "Descript", Descript);
                    value = cJSON_CreateNumber(msg->value);
                    cJSON_AddItemToObject(Event, "value", value);

                    stringJS = cJSON_Print(Event);
                    if (stringJS == NULL)
                    {                        
                        ESP_LOGE("Cloud", "Failed to create json Event.\n");
                    }
                    cJSON_Delete(Event);                    
                    return stringJS;
}

void cloud_loop(void)
{

    IOTHUB_CLIENT_LL_HANDLE iotHubClientHandle;
    EVENT_INSTANCE message;
    int receiveContext = 0;

    const char *connectionString = USER_CONNECTION_STRING2;

    ESP_LOGI("Cloud", "Connection String: %s", connectionString);

    

    if (platform_init() != 0)
    {
        ESP_LOGI("Cloud", "Failed to initialize the platform\r\n");
    }
    else
    {
        if ((iotHubClientHandle = IoTHubClient_LL_CreateFromConnectionString(connectionString, MQTT_Protocol)) == NULL)
        {
            ESP_LOGI("Cloud", "ERROR: iotHubClientHandle is NULL!\r\n");
        }
        else
        {
            bool traceOn = true;
            IoTHubClient_LL_SetOption(iotHubClientHandle, OPTION_LOG_TRACE, &traceOn);
            IoTHubClient_LL_SetConnectionStatusCallback(iotHubClientHandle, connection_status_callback, NULL);

#ifdef SET_TRUSTED_CERT_IN_SAMPLES
            IoTHubDeviceClient_LL_SetOption(iotHubClientHandle, OPTION_TRUSTED_CERT, certificates);
#endif // SET_TRUSTED_CERT_IN_SAMPLES

            // Set callback for handling cloud-to-device messages
            if (IoTHubClient_LL_SetMessageCallback(iotHubClientHandle, ReceiveMessageCallback, &receiveContext) != IOTHUB_CLIENT_OK)
            {
                printf("ERROR: IoTHubClient_LL_SetMessageCallback..........FAILED!\r\n");
            }

            int iterator = 0;
            time_t sent_time = 0;
            time_t current_time = 0;
            telemetry_t data[4];
            eventMsg_t eventMsg;
            while (1)
            {
                time(&current_time);

                if (iterator <= callbackCounter && (difftime(current_time, sent_time) > ((10000) / 1000)))
                {
                    char *strJS = NULL;  
                    if(xQueueReceive(Q_Measurement, &(data) , pdMS_TO_TICKS(1) ) == pdTRUE)
                    {
                        ESP_LOGI("Cloud", "INFO: there is a msg into queue telemetry\n");
                        strJS = create_telemetry(data);
                        if (strJS  !=  NULL)
                        {
                            strcpy(msgText, strJS);
                            ESP_LOGW("Cloud", "JSON: %s\n", strJS);
                        }
                        
                    }else if (xQueueReceive(Q_Event, &(eventMsg) , pdMS_TO_TICKS(1) ) == pdTRUE)
                    {
                        ESP_LOGI("Cloud", "INFO: there is a msg into queue event\n");
                        strJS = create_event_azure(&eventMsg);
                        if (strJS  !=  NULL)
                        {
                            strcpy(msgText, strJS);
                            ESP_LOGI("Cloud", "JSON event: %s\n", strJS);
                        }
                    }
                    
                     
                    //sprintf_s(msgText, sizeof(msgText), "{\"id\": 1}");
                    if ((message.messageHandle = IoTHubMessage_CreateFromByteArray((const unsigned char *)msgText, strlen(msgText))) == NULL)
                    {
                        ESP_LOGI("Cloud", "ERROR: iotHubMessageHandle is NULL! or no message ready to send\n");
                    }
                    else
                    {
                        message.messageTrackingId = iterator;
                        if (IoTHubClient_LL_SendEventAsync(iotHubClientHandle, message.messageHandle, SendConfirmationCallback, &message) != IOTHUB_CLIENT_OK)
                        {
                            ESP_LOGI("Cloud", "ERROR: IoTHubClient_LL_SendEventAsync..........FAILED!\r\n");
                        }
                        else
                        {
                            time(&sent_time);
                            ESP_LOGI("Cloud", "IoTHubClient_LL_SendEventAsync accepted message[%d] for transmission to IoT Hub.\r\n", iterator);
                        }
                    }
                    iterator++;
                }
                IoTHubClient_LL_DoWork(iotHubClientHandle);
                ThreadAPI_Sleep(10);
            }
        }
        IoTHubClient_LL_Destroy(iotHubClientHandle);
        platform_deinit();
        vTaskDelete(NULL);
    }
}
