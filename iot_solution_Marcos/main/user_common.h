#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

#include "esp_types.h"
#include "esp_err.h"
    esp_err_t check_connectivity_by_ping(const char *host);
    void cloud_loop(void);
    void ethernet_init(void);

    typedef struct
    {
        float holding_data0;
        float holding_data1;
        float holding_data2;
        float holding_data3;    
        float holding_data4;
        float holding_data5;
        float holding_data6;
        float holding_data7;
        float holding_data8;
        float holding_data9;
        uint16_t test_regs[150];
    } holding_reg_params_temp;

    typedef struct
    {
        float VL1N;
        float VL2N;
        float VL3N;
        float CL1;
        float CL2;
        float CL3;
        float FP1;
        float FP2;
        float FP3;
        float EAC;
    }telemetry_t;

    typedef struct
    {
        float value;
        int cid;
    }eventMsg_t;

    float buff_telemetry[10];

#ifdef __cplusplus
}
#endif
